import unittest
from TestUtils import TestCodeGen
from AST import *


class CheckCodeGenSuite(unittest.TestCase):
    def test_add_op(self):
        input = """
        var qswe:real;
        procedure main();
        begin
            putIntLn(-3);
            putIntLn(return5(-4));
            abc();
            pUTintLn(a);
            qswe := a;
            pUtFlOATLN(qsWe);
            puTboolLn(i);
            pUTfLoAtLn(return5real());
        end
        var a:integer;

        function return5(a:integer):integer;
        begin
            return 10-5;
        end

        function return5real():real;
        begin
            return (10000-5)*0+5;
        end

        function abs(n:integer):integer;
        begin
            //if a<0 then return -a;
            //else return a;
            return 0;
        end

        procedure abc();
        begin
            i := fALsE;
            a:=4;
        end
        var i:boolean;
        """
        expect = "-3\n5\n4\n4.0\nfalse\n5.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,500))

    def test_string(self):
        input = """
        procedure main();
        begin
            putstringln(returnsTRing("assd"));
        end

        function returnString(a: sTrIng):string;
        var b:strING;
        begin
            b:="ewrtgasd";
            return b;
        end
        var q:integer;
        """
        expect = "ewrtgasd\n"
        self.assertTrue(TestCodeGen.test(input,expect,501))

    def test_giaithua(self):
        input = """
        procedure main();
        begin
            putinT(Gt(10));
        end

        function gT(n:integer):inteGeR;
        var i,p:integer;
        begin
            p := 1;
            for i := 1 to n do
                p := p * i;
            return p;
        end

        var q:integer;
        """
        expect = "3628800"
        self.assertTrue(TestCodeGen.test(input,expect,502))

    def test_giaithua_de_quy(self):
        input = """
        procedure main();
        begin
            putinT(Gt(5));
        end

        function gt(n:integer):integer;
        begin
            if n=0 then return 1;
            else return n*gT(n-1);
        end

        var q:integer;
        """
        expect = "120"
        self.assertTrue(TestCodeGen.test(input,expect,503))

    def test_if(self):
        input = """
        procedure main();
        var n:integer;
        begin
            n:=0;
            if 0=n then n:=n+1;
            else n:=n+2;
            n:=n+4;
            putinT(N);
        end
        """
        expect = "5"
        self.assertTrue(TestCodeGen.test(input,expect,504))

    def test_bin_op_all(self):
        input = """
            procedure main();
            begin
                putintln(9 + 6);
                putintln(9 - 6);
                putintln(9 * 6);
                putintln(9 div 6);
                putintln(9 mod 6);
                putboolln(9 = 6);
                putboolln(9 <= 6);
                putboolln(9 < 6);
                putboolln(9 > 6);
                putboolln(9 >= 6);
                putboolln(9 <> 6);
                putfloatln(9 / 6);
                putfloatln(3.14159 + 2.51);
                putfloatln(3.14159 - 2.51);
                putfloatln(3.14159 * 2.51);
                putfloatln(3.14159 / 2.51);
                putboolln(3.14159 = 2.51);
                putboolln(3.14159 <= 2.51);
                putboolln(3.14159 < 2.51);
                putboolln(3.14159 > 2.51);
                putboolln(3.14159 >= 2.51);
                putboolln(3.14159 <> 2.51);
                putfloatln(3.14159 / 2.51);
                putBooLlN(trUe aNd (1.3>4));
                putBooLlN(trUe oR (1.3>4));
                putBooLlN(trUe aNd tHen (1.3>4));
                putBooLlN(trUe or eLse (1.3>4));
                putfloatln(3.14159 + 2);
                putfloat(3 + 2.51);
            end
        """
        expect = "15\n3\n54\n1\n3\nfalse\nfalse\nfalse\ntrue\ntrue\ntrue\n1.5\n5.6515903\n0.6315901\n7.885391\n1.2516296\nfalse\nfalse\nfalse\ntrue\ntrue\ntrue\n1.2516296\nfalse\ntrue\nfalse\ntrue\n5.14159\n5.51"
        self.assertTrue(TestCodeGen.test(input,expect,505))

    def test_unaryop_all(self):
        input = """
            procedure main();
            begin
                putintln(-3);
                putintln(--3);
                putfloaTln(-3.3);
                putboOlln(not trUe);
                putboOl(not (1 = 2));
            end
        """
        expect = "-3\n3\n-3.3\nfalse\ntrue"
        self.assertTrue(TestCodeGen.test(input,expect,506))

    def test_assoc_bin_and_unaryop(self):
        input = """
            procedure main();
            begin
                putfloaTln(-3 + 3.4);
                putfloaTln(--2.2 + -2);
            end
        """
        expect = "0.4000001\n0.20000005\n"
        self.assertTrue(TestCodeGen.test(input,expect,507))

    def test_simple_if(self):
        input = """
            var a:integer;
            procedure main();
            begin
                if true then PutInt(3);
                else puTiNt(2);
            end
            """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,508))

    def test_nested_if(self):
        input = """
            var a:integer;
            procedure main();
            begin
                if trUe then
                    if FaLsE then putiNt(5);
                    else putFloAt(5.2);
                else puTiNt(2);
            end
            """
        expect = "5.2"
        self.assertTrue(TestCodeGen.test(input,expect,509))

    def test_ambigous_if(self):
        input = """
            var a:integer;
            procedure main();
            begin
                if truE then
                    if fAlse then putInt(2);
                    else puTiNt(3);
            end
            """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,510))

    def test_complex_if(self):
        input = """
            var a,b:integer;
            procedure main();
            begin
                a:=0;
                if truE then a:=a+1;
                else a:=a+2;
                a:=a+4;
                b:=a;
                puTinT(b);
            end
            """
        expect = "5"
        self.assertTrue(TestCodeGen.test(input,expect,511))

    def test_if_not_else(self):
        input = """
            procedure main();
            begin
                putint(a()+b());
            end
            function a():integer;
            begin
                if truE then return 3;
                return 7;
            end
            function b():integer;
            begin
                if faLsE then return 3;
                return 7;
            end
            """
        expect = "10"
        self.assertTrue(TestCodeGen.test(input,expect,512))

    def test_break(self):
        input = """
            procedure main();
            var s,i:integer;
            begin
                s:=0;
                for i:=1 to 10 do
                    begin
                        s:=s+i;
                        if i=6 then break;
                    end
                putint(s);
            end
            """
        expect = "21"
        self.assertTrue(TestCodeGen.test(input,expect,513))

    def test_continue(self):
        input = """
            procedure main();
            var s,i:integer;
            begin
                s:=0;
                for i:=1 to 10 do
                    begin
                        if i=6 then continue;
                        s:=s+i;
                    end
                putint(s);
            end
            """
        expect = "49"
        self.assertTrue(TestCodeGen.test(input,expect,514))

    def test_complex_continue(self):
        input = """
            procedure main();
            var s,s1,i,j:integer;
            begin
                s:=0;
                for i:=1 to 10 do
                    begin
                        s1:=i;
                        for j:=1 to 100 do
                            begin
                                if (50<j) and (j<75) then continue;
                                s1 := s1 + j;
                            end
                        for j:=1000 downto 1 do 
                            begin
                                if (j<1000) then continue;
                                s1 :=s1-j;
                                continue;
                            end
                        if i=6 then continue;
                        s:=s+s1;
                        continue;
                    end
                putint(s);
            end
            """
        expect = "22999"
        self.assertTrue(TestCodeGen.test(input,expect,515))

    def test_simple(self):
        input = """
            var q,w:integer;
            procedure main();
            var a:integer;
            begin
                for a:=1 to 3 do 
                begin
                    putint(1);
                    if a>1 then break;
                end
            end
        """
        expect = "11"
        self.assertTrue(TestCodeGen.test(input,expect,516))

    def test_while(self):
        input = """
        procedure main();
        var i,s:integer;
        begin
            i:=s:=0;
            while i<=10 do
            begin
                s := s+i;
                i:=i+1;
            end
            s:=s*2;
            putiNt(s);
            return;
        end
        """
        expect = "110"
        self.assertTrue(TestCodeGen.test(input,expect,517))

    def test_with(self):
        input = """
            procedure main();
            var a,b:integer;
            begin
                a:=5;
                b:=999;
                putint(a);
                with a:real;do
                    begin
                        a:=6;
                        putfloAt(a);
                        putinT(b);
                    end
                putint(a);
            end
            var a:integer;
        """
        expect = "56.09995"
        self.assertTrue(TestCodeGen.test(input,expect,518))

    def test_andthen(self):
        input = """
            procedure main();
            begin
                if testShortCircuitHandled() then putstRing("Short circuit handled.");
                else putstRing("Short circuit did not handled.");
            end
            function testShortCircuitHandled():booleAn;
            var a_temp:integer;b:boolean;
            begin
                a_temp := a := 5;
                return (false and then (plusplusa() < 0)) or (a = a_temp);
            end
            function PlusPlusA():integer;
            begin
                a:=a+1;
                return a;
            end
            function APlusPlus():integer;
            var q:integer;
            begin
                q:=a;
                a:=a+1;
                return q;
            end
            var a:integer;
        """
        expect = "Short circuit handled."
        self.assertTrue(TestCodeGen.test(input,expect,519))

    def test_orelse(self):
        input = """
            procedure main();
            begin
                if testShortCircuitHandled() then putstRing("Short circuit handled.");
                else putstRing("Short circuit did not handled.");
            end
            function testShortCircuitHandled():booleAn;
            var a_temp:integer;b:boolean;
            begin
                a_temp := a := 5;
                return (true or else (plusplusa() < 0)) and (a = a_temp);
            end
            function PlusPlusA():integer;
            begin
                a:=a+1;
                return a;
            end
            function APlusPlus():integer;
            var q:integer;
            begin
                q:=a;
                a:=a+1;
                return q;
            end
            var a:integer;
        """
        expect = "Short circuit handled."
        self.assertTrue(TestCodeGen.test(input,expect,520))

    def test_coercions_param(self):
        input = """
            procedure main();
            begin
                putfLoAtLN(4);
                putFloaT(intToFloat(5));
            end
            function intToFloat(n:integer):real;
            begin
                return n;
            end
        """
        expect = "4.0\n5.0"
        self.assertTrue(TestCodeGen.test(input,expect,521))

    def test_passingparam(self):
        input = """
            procedure main();
            begin
                caller(1,2,3.0,tRUe, falsE = trUe,"day la string");
            end
            procedure caller(a:integer; b,c:real; d,e:boolean ;f:string);
            begin
                putiNt(a);
                putflOat(b);
                pUTFLoat(c);
                putBoOL(d);
                putBoOL(e);
                putstRing(f);
            end
        """
        expect = "12.03.0truefalseday la string"
        self.assertTrue(TestCodeGen.test(input,expect,522))

    def test_passingparam_function(self):
        input = """
            procedure main();
            begin
                putsTRing(caller(1,2,3.0,tRUe, falsE = trUe,"day la string"));
            end
            function caller(a:integer; b,c:real; d,e:boolean ;f:string):string;
            begin
                putiNt(a);
                putflOat(b);
                pUTFLoat(c);
                putBoOL(d);
                putBoOL(e);
                putstRing(f);
                return f;
            end
        """
        expect = "12.03.0truefalseday la stringday la string"
        self.assertTrue(TestCodeGen.test(input,expect,523))

    def test_for_downto(self):
        input = """
            procedure main();
            var i:integer;
            begin
                for i:=10 downto 1 do putiNtlN(i);
            end
        """
        expect = "10\n9\n8\n7\n6\n5\n4\n3\n2\n1\n"
        self.assertTrue(TestCodeGen.test(input,expect,524))

    def test_add_op_large(self):
        """Simple program: int main() {} """
        input = """procedure main(); begin putInt(32767+32767); end"""
        expect = "65534"
        self.assertTrue(TestCodeGen.test(input,expect,525))

    def test_mul_op(self):
        input =  """procedure main(); begin putInt(4*6); end"""
        expect = "24"
        self.assertTrue(TestCodeGen.test(input,expect,526))

    def test_sub_op(self):
        input =  """procedure main(); begin putInt(6-32767); end"""
        expect = "-32761"
        self.assertTrue(TestCodeGen.test(input,expect,527))

    def test_add_op_float(self):
        """Simple program: int main() {} """
        input = """procedure main(); begin putFloat(100.5+40); end"""
        expect = "140.5"
        self.assertTrue(TestCodeGen.test(input,expect,528))

    def test_add_op_large_float(self):
        """Simple program: int main() {} """
        input = """procedure main(); begin putFloat(32766.9+32766.5); end"""
        expect = "65533.4"
        self.assertTrue(TestCodeGen.test(input,expect,529))

    def test_mul_op_float(self):
        input =  """procedure main(); begin putFloat(4e3*6.5); end"""
        expect = "26000.0"
        self.assertTrue(TestCodeGen.test(input,expect,530))

    def test_sub_op_float(self):
        input =  """procedure main(); begin putFloat(6-327.67); end"""
        expect = "-321.67"
        self.assertTrue(TestCodeGen.test(input,expect,531))

    def test_div_op_1_float(self):
        input =  """procedure main(); begin putFloat(6/327.67); end"""
        expect = "0.018311106"
        self.assertTrue(TestCodeGen.test(input,expect,532))

    def test_div_op_2_float(self):
        input =  """procedure main(); begin putFloat(6.5/327.67); end"""
        expect = "0.019837031"
        self.assertTrue(TestCodeGen.test(input,expect,533))

    def test_div_op_2_int(self):
        input =  """procedure main(); begin putFloat(9/5); end"""
        expect = "1.8"
        self.assertTrue(TestCodeGen.test(input,expect,534))
        
    def test_ppa(self):
        input = """
            var a,b,c,d:integer;
            procedure main();
            begin
                b:=c:=d:=pluSPLUsA();
                putinT(a);
                putinT(b);
                putinT(c);
                putinT(d);
                putinT(pluSPLUsA());
                putinT(pluSPLUsA());
                putinT(pluSPLUsA());
                putinT(pluSPLUsA());
            end
            function PlusPlusA():integer;
            begin
                a:=a+1;
                return a;
            end
        """
        expect = "11112345"
        self.assertTrue(TestCodeGen.test(input,expect,535))

    def test_float_compare(self):
        input = """
             procedure main();
             begin
                 putBool(1.23 <> 1.23);
                 putBool(1.23 <> 2.1);
                 putBool(1.23 < 2.19);
                 putBool(1.23 < 0.2);
                 putBool(1.23 > 2.11);
                 putBool(1.23 > (-0.1));
                 putBool(1.23 = 2.12);
                 putBool(1.23 = (1 + 0.23));
                 putBool(1.23 >= 2.1);
                 putBool(1.23 >= 1);
                 putBool(1.23 >= 1.23);
                 putBool(1.23 <= 2.1);
                 putBool(1.23 <= 0.12);
                 putBool(1.23 <= 1.23);
             end
        """
        expect = "falsetruetruefalsefalsetruefalsetruefalsetruetruetruefalsetrue"
        self.assertTrue(TestCodeGen.test(input,expect,536))

    def test_while_stmt__1(self):
        input = """
             procedure main();
             var i,s: integer;
             begin
                i := 1;
                s := 0;
                while (i <= 10) do
                begin
                    s := s + i;
                    break;
                    i := i + 1;
                end
                putInt(s);
             end
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,537))

    def test_for_stmt__1(self):
        input = """
             procedure main();
             var i,s: integer;
             begin
                s := 0;
                for i := 10 downto 1 do
                begin
                    s := s + i;
                    break;
                end
                putInt(s);
             end
        """
        expect = "10"
        self.assertTrue(TestCodeGen.test(input,expect,538))

    def test_compicate_if(self):
        input = """
        function foo(): boolean;
        begin
            putStringLn("hello");
            return true;
        end

        procedure main();
        var x : integer;
        begin
            x := 123;
            if (x > 100) then
                putStringLn("100 < x < 200");
            else if (x > 200) then
                putStringLn("200 < x < 300");
            else if (x > 300) then
                putStringLn("x > 300");
            else 
            begin
                if (x > 100) then
                 x := x + 100;
            end
            putIntLn(x);
        end
"""
        expect = """100 < x < 200
123
"""
        self.assertTrue(TestCodeGen.test(input, expect, 539))

    def test_hello(self):
        input = """
        function foo(): boolean;
        begin
            putStringLn("hello");
            return true;
        end

        procedure main();
        var x : integer;
        begin
            x := 123;
            if (true and false) then
                x := x - 100;
            putIntLn(x);
        end
        """
        expect = """123\n"""
        self.assertTrue(TestCodeGen.test(input, expect, 540))

    def test_while_lmao(self):
        input = """
        function foo(): boolean;
        begin
            putStringLn("hello");
            return true;
        end

        procedure main();
        var x : integer;
        begin
            x := 1;
            while x <= 10 do
            begin
 putIntLn(x);
                if x = 10 then putStringLn("lmao");

                x := x + 1;
            end
        end
        """
        expect = """1
2
3
4
5
6
7
8
9
10
lmao
"""
        self.assertTrue(TestCodeGen.test(input, expect, 541))

    def test_is_prime(self):
        input = """
function isPrime(x: integer): boolean;
var i : integer;
begin
    for i := 2 to x div 2 do
    begin
        if x mod i = 0 then return false;
    end

    return true;
end

procedure main();
var x, i : integer;
begin
    i := 0;
    x := 100;

    while i <= x do
    begin
        if isPrime(i) then putIntLn(i);
        i := i + 1;
    end

end
        """
        expect = """0
1
2
3
5
7
11
13
17
19
23
29
31
37
41
43
47
53
59
61
67
71
73
79
83
89
97
"""
        self.assertTrue(TestCodeGen.test(input, expect, 542))

    def test_fact(self):
        input = """
function fact(x: integer): integer;
var i, f : integer;
begin
    f := 1;
    for i := 1 to x do f := f * i;
    return f;
end

procedure main();
var s, i : integer;
begin
    i := 1;
    s := 0;

    while i <= 10 do 
    begin
        putIntLn(fact(i));
        i := i + 1;
    end

end
        """
        expect = """1
2
6
24
120
720
5040
40320
362880
3628800
"""
        self.assertTrue(TestCodeGen.test(input, expect, 543))

    def testnested_with(self):
        input = """
       procedure main();
       var x: integer;
       begin
            x := 1;
            with x : integer; do 
            begin
                x := 2;
                with x : integer; do
                begin
                    x := 3;
                    putInt(x);
                end
                putInt(x);
            end
       end
        """
        expect = """32"""
        self.assertTrue(TestCodeGen.test(input, expect, 544))

    def test_if_then_else(self):
        input = """

    var x: integer;
    
    function foo(): boolean;
    begin
        putString("in foo");
        return false;
    end

    procedure main();
    begin
        x := 10;
        if (x > 100 and then foo()) then
            putStringLn("in then");
        else
            putStringLn("in else");
    end
        """
        expect = """in else\n"""
        self.assertTrue(TestCodeGen.test(input, expect, 545))

    def test_if_then_elsse(self):
        input = """
    var x: integer;
    
    function foo(): boolean;
    begin
        putString("in foo ");
        x := 1000;
        return true;
    end

    procedure main();
    begin
        x := 10;

        if (x < 100 and then foo()) then
            putStringLn("in then");
        else
            putStringLn("in else");
    end        """
        expect = """in foo in then\n"""
        self.assertTrue(TestCodeGen.test(input, expect, 546))

    def test_in_foo(self):
        input = """
    var x: integer;
    
    function foo(): boolean;
    begin
        putString("in foo ");
        return false;
    end

    procedure main();
    begin
        x := 10;

        if (x < 100 and then foo()) then
            putStringLn("in then");
        else
            putStringLn("in else");
    end        """
        expect = """in foo in else\n"""
        self.assertTrue(TestCodeGen.test(input, expect, 547))

    def test_nice(self):
        input = """
    var x: integer;
    
    function foo(): boolean;
    begin
        putString("in foo ");
        x := 1000;
        return true;
    end

    procedure main();
    begin
        x := 10;

        if (x > 100 and then foo()) then
            putStringLn("in then");
        else
            putStringLn("in else");

        putIntLn(x);
    end        """
        expect = """in else\n10\n"""
        self.assertTrue(TestCodeGen.test(input, expect, 548))

    def test_real(self):
        input = """
       function foo(): real;
       begin
            return 1;
        end

        procedure main();
        begin
            putFloat(foo());
        end
        """
        expect = """1.0"""
        self.assertTrue(TestCodeGen.test(input, expect, 549))

    def test_gcd(self):
        input = """
        procedure main();
        var x, y: integer;
        begin
            x := 10;
            y := 12;
            putInt(gcd(x + y, x));
        end

        function gcd(a,b : integer): integer;
        begin
            if b = 0 then
                return a;
            else
                return gcd(b, a mod b);
        end        """
        expect = """2"""
        self.assertTrue(TestCodeGen.test(input, expect, 550))

    def test_assign(self):
        input = """
            var d: integer;
            procedure main();
            var a,b: boolean;
                c: String;
            begin
                a := True;
                b := False;
                c := "ahihi";
                d := 1 + 2;
                putBool(a or b and not b or False);
                putString(c);
                putInt(d);
            end
        """
        expect = "trueahihi3"
        self.assertTrue(TestCodeGen.test(input, expect, 551))

    def test_putint(self):
        input = """
        procedure main();
        var X: integer;
        begin
            x := 12;
            putint(X);
            Foo();
        end

        procedure foo();
        begin
            putString("Hello world");
        end
        """
        expect = """12Hello world"""
        self.assertTrue(TestCodeGen.test(input, expect, 552))

    def test_factor(self):
        input = """
            procedure main();
            var a,b: integer;
            begin
                b := 6;
                a := factor(b);
                putInt(a);
            end
            function factor(a: integer): integer;
            begin
                if a <= 1 then
                    return 1;
                else
                    return a * factor(a-1);
            end
        """
        expect = """720"""
        self.assertTrue(TestCodeGen.test(input, expect, 553))

    def test_scope1(self):
        input = """
            procedure main();
            var a: integer;
                b: real;
            begin
                a := 1;
                putInt(a);
                with a: real; b: integer; do begin
                    a := 1.5;
                    b := 1;
                    putFloat(a+b+0.15);
                end
                with a: boolean ; b: boolean; do begin
                    b := true;
                    a := b;
                    putBool(a);
                end
                a := a + 2;
                putInt(3);
            end
        """
        expect = "12.65true3"
        self.assertTrue(TestCodeGen.test(input, expect, 554))

    def test_while1(self):
        input = """
            procedure main();
            var a,i: integer;
                b: real;
            begin
                i := 8 ;
                a := 1 ;
                while (i>0) do begin
                    a := a * i;
                    i := i - 1;
                    if i = 4 then break;
                end
                putInt(a);
            end
            """
        expect = "1680"
        self.assertTrue(TestCodeGen.test(input, expect, 555))

    def test_bool_ast5(self):
        input = """
            procedure main();
            var a,b: boolean;
            begin
                a := True;
                b := False;
                putbool(a and b and then a and not b and test());
            end
            function test(): boolean;
            var a: real;
                res: boolean;
            begin
                res := false;
                a := 9.5;
                putFloat(a);
                return res;
            end
            """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 556))

    def test_bool_ast6(self):
        input = """
            procedure main();
            var a,b: boolean;
            begin
                a := True;
                b := False;
                putBool((a or test()) or else a and not b and test());
            end
            function test(): boolean;
            var a: real;
                res: boolean;
            begin
                res := false;
                a := 9.5;
                putFloat(a);
                return res;
            end
            """
        expect = "9.5true"
        self.assertTrue(TestCodeGen.test(input, expect, 557))

    def test_for1(self):
        input = """
            procedure main();
            var a,i: integer;
                b: real;

            begin
                up := 10;
                a := 0;
                for i:=up downto 1 do begin
                    if a > 40 then continue;
                    a := a + i;
                end
                putInt(a);
            end
            var up:integer;
            """
        expect = "45"
        self.assertTrue(TestCodeGen.test(input, expect, 558))

    def test_call_stmt1(self):
        input = """
            procedure main();
            var a,b: integer;
            begin
                b := 6;
                a := factor(b);
                putInt(a);
            end
            function factor(a: integer): integer;
            begin
                if a <= 1 then
                    return 1;
                else
                    return a * factor(a-1);
            end
        """
        expect = "720"
        self.assertTrue(TestCodeGen.test(input, expect, 559))

    def test_empty(self):
        input = """
        procedure main();
        begin
        end
        """
        expect = """"""
        self.assertTrue(TestCodeGen.test(input, expect, 560))
    def test_int(self):
        input = """
        procedure main();
        var a:integer;
        begin
        a := 1;
        putInt(a);
        end
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,561))

    def test_tree(self):
        input = """
        procedure main();
        begin
        putFloat(100.02);
        end
        """
        expect = "100.02"
        self.assertTrue(TestCodeGen.test(input,expect,562))

    def test_putfloat(self):
        input = """
        procedure main();
        begin
        putFloat(1.4315E7);
        end
        """
        expect = "1.4315E7"
        self.assertTrue(TestCodeGen.test(input,expect,563))

    def test_float_and_putfloat(self):
        input = """
        procedure main();
        begin
        putFloat(121.5E5);
        end
        """
        expect = "1.215E7"
        self.assertTrue(TestCodeGen.test(input,expect,564))

    def test_two_putint(self):
        input = """
        procedure main();
        begin
            if (true)
                then putInt(1);
                else putInt(2);
        end
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,565))

    def test_foo(self):
        input = """
        procedure main();
        var a:integer;
        begin
            a := 4;
            putFloatLn(foo(a));
        end
        function foo(a:integer):real;
        var foo:integer;
        begin
            foo := 5;
            return foo + a;
        end 
        """
        expect = """9.0\n"""
        self.assertTrue(TestCodeGen.test(input,expect,566))

    def test_putln(self):
        input = """
        procedure main();
        begin
            putIntLn(000);
            PuTlN();
        end
        """
        expect = "0\n\n"
        self.assertTrue(TestCodeGen.test(input,expect,567))

    def test_float_put(self):
        input = """
        procedure main();
        begin
            putFloatLn(1.0);
        end"""
        expect = "1.0\n"
        self.assertTrue(TestCodeGen.test(input,expect,568))

    def test_put_float(self):
        input = """
        procedure main();
        begin
            putFloatLn(10.5);
        end
        """
        expect = "10.5\n"
        self.assertTrue(TestCodeGen.test(input,expect,569))
        
    def test_putfloatln(self):
        input = """
        procedure main();
        begin
            putFloatLn(100.14);
        end
        """
        expect = "100.14\n"
        self.assertTrue(TestCodeGen.test(input,expect,570))

    def test_putbool(self):
        input = """
        procedure main();
        begin
            putBoolLn(true);
        end
        """
        expect = "true\n"
        self.assertTrue(TestCodeGen.test(input,expect,571))

    def test_manipulate(self):
        input = """
        procedure main();
        var a:integer;
        begin
            a := 2;
            if a > 5 then
                if a mod 2=0 then
                    a := a * 2;
                else
                begin
                end
            else begin
                a := 11;
                if a mod 3 <> 0 then
                    a := a * 3 div 2;
            end
            putInt(a);
        end
        """
        expect = "16"
        self.assertTrue(TestCodeGen.test(input,expect,572))

    def test_dowhile_stmt_simple(self):
        input = """
        procedure main();
        var a:integer;
        begin
            a := 1;
            while a < 5 do
            begin
                putInt(a);
                a := a + 1;
            end
        end
        """
        expect = "1234"
        self.assertTrue(TestCodeGen.test(input,expect,573))

    def test_manipulate_continue(self):
        input = """
        procedure main();
        var a, iSum:integer;
        begin
            a := 0;
            iSum := 0;
            while a < 20 do
            begin
                a := a + 1;
                if a mod 2=0 then continue;
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "100"
        self.assertTrue(TestCodeGen.test(input,expect,574))
        
    def test_while_has_break(self):
        input = """
        procedure main();
        var a, iSum:integer;
        begin
            a := 0;
            iSum := 0;
            while a < 20 do
            begin
                a := a + 1;
                if a > 17 then break;
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "153"
        self.assertTrue(TestCodeGen.test(input,expect,575))

    def test_while_has_break_and_continue(self):
        input = """
        procedure main();
        var a, iSum:integer;
        begin
            a := 0;
            iSum := 0;
            while a < 20 do
            begin
                a := a + 1;
                if a > 17 then break;
                if a mod 2=0 then continue;
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "81"
        self.assertTrue(TestCodeGen.test(input,expect,576))

    def test_inner(self):
        input = """
        procedure main();
        var a, b, iSum:integer;
        begin
            a := b := iSum := 0;
            while a < 20 do
            begin
                b := 0;
                a := a + 1;
                while b < a do
                begin
                    b := b + 1;
                    iSum := iSum + b;
                end
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "1750"
        self.assertTrue(TestCodeGen.test(input,expect,577))

    def test_continue_and_break(self):
        input = """
        procedure main();
        var a, b, iSum:integer;
        begin
            a := b := iSum := 0;
            while a < 20 do
            begin
                b := 0;
                a := a + 1;
                while b < a do
                begin
                    b := b + 1;
                    if b > 10 then break;
                    if b mod 2=1 then continue;
                    iSum := iSum + b;
                end
                if a mod b=0 then continue;
                if a + b > 40 then break;
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "554"
        self.assertTrue(TestCodeGen.test(input,expect,578))

    def test_putint_in_for(self):
        input = """
        procedure main();
        var a:integer;
        begin
            for a := 0 to 10 do
            begin
                putInt(a);
                break;
            end
        end
        """
        expect = "0"
        self.assertTrue(TestCodeGen.test(input,expect,579))
        
    def test_sum(self):
        input = """
        procedure main();
        var a, b, iSum:integer;
        begin
            iSum := 0;
            for a := 0 to 9 do
            begin
                if a mod 2=0 then continue;
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "25"
        self.assertTrue(TestCodeGen.test(input,expect,580))

    def test_break_sum(self):
        input = """
        procedure main();
        var a, b, iSum:integer;
        begin
            iSum := 0;
            for a := 0 to 9 do
            begin
                if iSum > 27 then break;
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "28"
        self.assertTrue(TestCodeGen.test(input,expect,581))

    def test_isum(self):
        input = """
        procedure main();
        var a, b, iSum:integer;
        begin
            iSum := 0;
            for a := 0 to 9 do
            begin
                if iSum > 27 then break;
                if a mod 3=0 then continue;
                iSum := iSum + a;
            end
            putInt(iSum);
        end
        """
        expect = "27"
        self.assertTrue(TestCodeGen.test(input,expect,582))

    def test_isum_compicate(self):
        input = """procedure main();
        var a, b, iSum:integer;
        begin
            iSum := 0;
            for a := 0 to 9 do
            begin
                for b := 0 to a - 1 do
                begin
                    if a + b > 17 then break;
                    if b mod 2=0 then continue;
                    iSum := iSum + b;
                end
                if iSum > 27 then break;
                if a mod 3 <> 0 then continue;
                iSum := iSum + a;
            end
            putIntLn(iSum);
        end
        """
        expect = "37\n"
        self.assertTrue(TestCodeGen.test(input,expect,583))

    def test_put_Float(self):
        input = """
        var i, j:integer;
        procedure main();
        var a, b, iSum:integer;
        begin
            i := 10;
            with i:real; do
            begin
                i := 11.8;
                putFloat(i);
            end
            i := 11;
            putIntLn(i);
        end
        """
        expect = "11.811\n"
        self.assertTrue(TestCodeGen.test(input,expect,584))
        
    def test_putInt(self):
        """Program => manipulate data in Main function: block inner block"""
        input = """
        var i, j:integer;
        procedure main();
        var a, b, iSum:integer;
        begin
            i := 10;
            with i:real; do
            begin
                i := 14.3;
                with i:integer; do
                begin
                    i := 19;
                    putInt(i);
                end
                putFloat(i);
            end
            putInt(i);
        end
        """
        expect = "1914.310" 
        self.assertTrue(TestCodeGen.test(input,expect,585))

    def test_PutFloat(self):
        """Program => Funcall is stmt in main function"""
        input = """
        var a:integer;
        procedure main();
        var b:integer;
            c:real;
        begin
            b := 5;
            c := foo(b);
            putFloat(c);
        end
        
        function foo(a:integer):integer;
        begin
            return a * a;
        end
        """
        expect = "25.0"
        self.assertTrue(TestCodeGen.test(input,expect,586))

    def test_compication_if(self):
        """Program => return stmt in if-else stmt in function call"""
        input = """
        procedure main();
        var a, b, res:integer;
        begin
            a := 1;
            b := 1;
            res := foo(a, b);
            putIntLn(res);
        end
        
        function foo(a:integer; b:integer):integer;
        begin
            if a=b
                then return 111;
                else return 222;
        end
        """
        expect = "111\n"
        self.assertTrue(TestCodeGen.test(input,expect,587))

    def test_put_Int(self):
        input = """
            procedure main();
            begin
                putIntLn(100);
                putIntLn(-100);
                putInt(0);
            end
        """
        expect = "100\n-100\n0"
        self.assertTrue(TestCodeGen.test(input,expect,588))

    def test_large_int_literal(self):
        input = """
            procedure main();
            begin
                putIntLn(-2147483648);
                putIntLn(-1000000000 + 65565654);
                putFloatLn(-1000000000 / 65565654);
                putIntLn(2147483647);   
            end
        """
        expect = "-2147483648\n-934434346\n-15.251887\n2147483647\n"
        self.assertTrue(TestCodeGen.test(input,expect,589))

    
    def test_float_literal(self):
        input = """
            procedure main();
            begin
                putFloatLn(10.0);
                putFloatLn(1e3);
                putFloatLn(1e-3);
                putFloat(1.e-3);
            end
        """
        expect = "10.0\n1000.0\n0.001\n0.001"
        self.assertTrue(TestCodeGen.test(input,expect,590))

    def test_boolean_literal(self):
        input = """
            procedure main();
            begin
                putBoolLn(True);
                putBool(FalsE);
            end
        """
        expect = "true\nfalse"
        self.assertTrue(TestCodeGen.test(input,expect,591))
    
    def test_string_literal(self):
        input = """
            procedure main();
            begin
                putStringLn("PPL");
                putString("2018");
            end
        """
        expect = "PPL\n2018"
        self.assertTrue(TestCodeGen.test(input,expect,592))

    def test_binary_op_with_add(self):
        input = """
            procedure main();
            begin
                putIntLn(1 + 2);
                putFloatLn(1 + 2.0);
                putFloatLn(1.23 + -9.34);
                putFloat(1.2376 + -9);
            end
        """
        expect = "3\n3.0\n-8.110001\n-7.7624"
        self.assertTrue(TestCodeGen.test(input,expect,593))

    def test_binary_op_with_sub(self):
        input = """
            procedure main();
            begin
                putIntLn(1 - 2);
                putFloatLn(1 - 2.0);
                putFloatLn(1.23 - -9.34);
                putFloat(1.2376 - -9);
            end
        """
        expect = "-1\n-1.0\n10.57\n10.2376"
        self.assertTrue(TestCodeGen.test(input,expect,594))

    def test_binary_op_with_divide(self):
        input = """
            procedure main();
            begin
                putFloatLn(14/2);
                putFloatLn(13 / 2.08783);
                putFloatLn(158.2035564 / --236.256885);
                putFloat(122.23762 / ---9);
            end
        """
        expect = "7.0\n6.2265606\n0.66962516\n-13.581958"
        self.assertTrue(TestCodeGen.test(input,expect,595))

    def test_binary_op_with_mul(self):
        input = """
            procedure main();
            begin
                putFloatLn(14/-2.0);
                putFloatLn(13 * 2.08783);
                putFloatLn(158.2035564 * -236.256885);
                putFloat(122.23762 * 9);
            end
        """
        expect = "-7.0\n27.14179\n-37376.68\n1100.1385"
        self.assertTrue(TestCodeGen.test(input,expect,596))


    def test_binary_op_add_int(self):
        input = """
            procedure main();
            begin
                putInt(1+2+30);
            end
        """
        expect = "33"
        self.assertTrue(TestCodeGen.test(input,expect,597))
    
    def test_binary_op_divide(self):
        input = """
            procedure main();
            begin
                putFloat(1*45+30/12);
            end
        """
        expect = "47.5"
        self.assertTrue(TestCodeGen.test(input,expect,598))
    
    def test_binary_op_div_int(self):
        input = """
            procedure main();
            begin
                putInt(100 div 3 div 2);
            end
        """
        expect = "16"
        self.assertTrue(TestCodeGen.test(input,expect,599))
