.source MPClass.java
.class public MPClass
.super java.lang.Object
.field static a I

.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String; from Label0 to Label1
Label0:
	invokestatic MPClass/testShortCircuitHandled()Z
	ifle Label2
	ldc "Short circuit handled."
	invokestatic io/putString(Ljava/lang/String;)V
	goto Label3
Label2:
	ldc "Short circuit did not handled."
	invokestatic io/putString(Ljava/lang/String;)V
Label3:
Label1:
	return
.limit stack 3
.limit locals 1
.end method

.method public static testShortCircuitHandled()Z
.var 0 is a_temp I from Label0 to Label1
.var 1 is b Z from Label0 to Label1
Label0:
	iconst_5
	putstatic MPClass/a I
	getstatic MPClass/a I
	istore_0
	iconst_1
	ifgt Label4
	invokestatic MPClass/PlusPlusA()I
	iconst_0
	if_icmpge Label2
	iconst_1
	goto Label3
Label2:
	iconst_0
Label3:
	ifgt Label4
	iconst_0
	goto Label5
Label4:
	iconst_1
Label5:
	getstatic MPClass/a I
	iload_0
	if_icmpne Label6
	iconst_1
	goto Label7
Label6:
	iconst_0
Label7:
	iand
	ireturn
Label1:
.limit stack 8
.limit locals 2
.end method

.method public static PlusPlusA()I
Label0:
	getstatic MPClass/a I
	iconst_1
	iadd
	putstatic MPClass/a I
	getstatic MPClass/a I
	ireturn
Label1:
.limit stack 2
.limit locals 0
.end method

.method public static APlusPlus()I
.var 0 is q I from Label0 to Label1
Label0:
	getstatic MPClass/a I
	istore_0
	getstatic MPClass/a I
	iconst_1
	iadd
	putstatic MPClass/a I
	iload_0
	ireturn
Label1:
.limit stack 2
.limit locals 1
.end method

.method public <init>()V
.var 0 is this LMPClass; from Label0 to Label1
Label0:
	aload_0
	invokespecial java/lang/Object/<init>()V
Label1:
	return
.limit stack 1
.limit locals 1
.end method
