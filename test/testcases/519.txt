
            procedure main();
            begin
                if testShortCircuitHandled() then putstRing("Short circuit handled.");
                else putstRing("Short circuit did not handled.");
            end
            function testShortCircuitHandled():booleAn;
            var a_temp:integer;b:boolean;
            begin
                a_temp := a := 5;
                return (false and then (plusplusa() < 0)) or (a = a_temp);
            end
            function PlusPlusA():integer;
            begin
                a:=a+1;
                return a;
            end
            function APlusPlus():integer;
            var q:integer;
            begin
                q:=a;
                a:=a+1;
                return q;
            end
            var a:integer;
        